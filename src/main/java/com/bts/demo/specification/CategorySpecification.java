package com.bts.demo.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.bts.demo.models.entities.Category;
import com.bts.demo.models.metaModels.Category_;

@Component
public class CategorySpecification {

	public Specification<Category> nameLike(String name){
		
	  return new Specification<Category>() {


	@Override
	public Predicate toPredicate(Root<Category> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		 return criteriaBuilder.like(root.get(Category_.NAME), "%"+name+"%");
	}
	  };
	}
}
