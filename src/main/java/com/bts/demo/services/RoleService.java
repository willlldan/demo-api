package com.bts.demo.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bts.demo.models.entities.Role;
import com.bts.demo.models.repository.RoleRepository;

@Service
@Transactional
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	
	public Role save(Role role) {
		return roleRepository.save(role);
	}
	
	public Role findOne(Long id) {
		Optional<Role> product = roleRepository.findById(id);
		
		return !product.isPresent()? null : product.get();
		
	}
	
	public Iterable<Role> findAll() {
		return roleRepository.findAll();
	}
	
	public void removeOne(Long id) {
		roleRepository.deleteById(id);
	}
	

}
