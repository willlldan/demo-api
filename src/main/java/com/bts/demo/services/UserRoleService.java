package com.bts.demo.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bts.demo.dto.UserRoleData;
import com.bts.demo.models.entities.UserRole;
import com.bts.demo.models.repository.AppUserRepository;
import com.bts.demo.models.repository.RoleRepository;
import com.bts.demo.models.repository.UserRoleRepository;


@Service
@Transactional
public class UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private AppUserRepository appUserRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	
	public UserRole save(UserRole userRole) {
		
		boolean checkAppUser = appUserRepository.findById(userRole.getUserRoleId().getUserId()).isPresent();
		boolean checkRole = roleRepository.findById(userRole.getUserRoleId().getRoleId()).isPresent();
		
		if(!checkAppUser || !checkRole) {
			return null;
		}
		
	
		
		return userRoleRepository.save(userRole);
	}
	
	public UserRole findOne(Long id) {
		Optional<UserRole> product = userRoleRepository.findById(id);
		
		return !product.isPresent()? null : product.get();
		
	}
	
	public Iterable<UserRole> findAll() {
		return userRoleRepository.findAll();
	}
	
	public void removeOne(UserRole id) {
		userRoleRepository.delete(id);
	}
}
