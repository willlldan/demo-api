package com.bts.demo.utils;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.bts.demo.models.entities.AppUser;

public class AuditorAwareImpl implements AuditorAware<String>{

	@Override
	public Optional<String> getCurrentAuditor() {
		
		try {
			AppUser currentUser = (AppUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return currentUser == null? Optional.of("Added by task scheduler") : Optional.of(currentUser.getEmail());
			
		} catch (Exception e) {
			// TODO: handle exception
			return Optional.of("SYSTEM") ;
		}
		
		
	}

}
