package com.bts.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.dto.ResponseData;
import com.bts.demo.dto.SearchData;
import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Product;
import com.bts.demo.services.CartService;

@RestController
@RequestMapping("/api/cart")
public class CartController {

	@Autowired
	private CartService cartService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<ResponseData<Cart>> save(@Valid @RequestBody Cart cartData, Errors errors) {
		ResponseData<Cart> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		};
		
		Cart cart = modelMapper.map(cartData, Cart.class);
		
		response.setStatus(true);
		response.setData(cartService.save(cart));
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<ResponseData<Iterable<Cart>>> findAll() {
		
		ResponseData<Iterable<Cart>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(cartService.findAll());
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseData<Cart>> findOne(@PathVariable("id") Long id) {
		
		ResponseData<Cart> response = new ResponseData<>();
		response.setStatus(true);
		response.setData(cartService.findOne(id));
		
		return ResponseEntity.ok(response);
		
	}
	
	@PutMapping
	public ResponseEntity<ResponseData<Cart>> update(@Valid @RequestBody Cart cartData, Errors errors) {
		ResponseData<Cart> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		};
		
		Cart cart = modelMapper.map(cartData, Cart.class);
		
		response.setStatus(true);
		response.setData(cartService.save(cart));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public void removeOne(@PathVariable("id") Long id) {
		cartService.removeOne(id);
	}
	
	@PostMapping("/{id}")
	public void addProduct(@RequestBody Product product, @PathVariable("id") Long cartId) {
		cartService.addProduct(product, cartId);
	}
	
	
//	find cart by product
	@GetMapping("/search/product/{id}")
	public List<Cart> findByProduct(@PathVariable("id") Long id) {
		return cartService.findCartByProduct(id);
	}
	
	@PostMapping("/search/email")
	public Cart findByEmail(@RequestBody SearchData searchData) {
		return cartService.findByEmail(searchData.getSearchKey());
	}
	
	@PostMapping("/search/name")
	public List<Cart> findByName(@RequestBody SearchData searchData) {
		return cartService.findByName(searchData.getSearchKey());
	}
	
	@PostMapping("/search/emailEndWith")
	public List<Cart> findByEmailEndWith(@RequestBody SearchData searchData) {
		return cartService.findByEmailEndWith(searchData.getSearchKey());
	}
	
	@PostMapping("/search/nameOrEmail")
	public List<Cart> findByNameOrEmail(@RequestBody SearchData searchData) {
		return cartService.findByNameOrEmail(searchData.getSearchKey(), searchData.getOtherSearchKey());
	}
	
	
	@PostMapping("/name")
	public List<Cart> findByNameSpec(@RequestBody SearchData searchData) {
		return cartService.findByNameSpec(searchData.getSearchKey());
	}
	

}
