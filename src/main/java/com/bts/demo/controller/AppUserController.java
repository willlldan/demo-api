package com.bts.demo.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.config.JwtTokenUtil;
import com.bts.demo.dto.AppUserData;
import com.bts.demo.dto.JwtData;
import com.bts.demo.dto.ResponseData;
import com.bts.demo.models.entities.AppUser;
import com.bts.demo.services.AppUserService;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
public class AppUserController {

	@Autowired
	AppUserService appUserService;
	
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@PostMapping("/register")
	public ResponseEntity<ResponseData<AppUser>> register(@RequestBody AppUserData userData) {
		ResponseData<AppUser> response = new ResponseData<>();
		AppUser appUser = modelMapper.map(userData, AppUser.class);
		response.setData(appUserService.register(appUser));
		response.setStatus(true);
		response.getMessage().add("Welcome " + appUser.getName() + ". Now you can access our service !!");
		
		return ResponseEntity.ok(response);	
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<JwtData> createAuthenticationToken(@RequestBody AppUserData appUserData) throws Exception {
		JwtData response = new JwtData();
		
		authenticate(appUserData.getEmail(), appUserData.getPassword());
		
		final UserDetails userDetails = appUserService.loadUserByUsername(appUserData.getEmail());
		
		final String token = jwtTokenUtil.generateToken(userDetails);
		
		response.setToken(token);
		
		return ResponseEntity.ok(response);
		
	}
	
	private void authenticate(String email, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	
}
