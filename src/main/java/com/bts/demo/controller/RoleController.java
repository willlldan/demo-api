package com.bts.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.dto.ResponseData;
import com.bts.demo.models.entities.Role;
import com.bts.demo.services.RoleService;

@RestController
@RequestMapping("/api/role")
public class RoleController {
	
	@Autowired
	private RoleService roleService;

	
	@GetMapping
	public ResponseEntity<ResponseData<Iterable<Role>>> findAll() {
		
		ResponseData<Iterable<Role>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(roleService.findAll());
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseData<Role>> findOne(@PathVariable("id") Long id) {
		
		ResponseData<Role> response = new ResponseData<>();
		response.setStatus(true);
		response.setData(roleService.findOne(id));
		
		return ResponseEntity.ok(response);
		
	}

	
	@PostMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Role>> save(@Valid @RequestBody Role role, Errors errors) {
		
		ResponseData<Role> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setStatus(true);
		response.setData(roleService.save(role));
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Role>> update(@Valid @RequestBody Role role, Errors errors) {

		ResponseData<Role> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setStatus(true);
		response.setData(roleService.save(role));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public void removeOne(@PathVariable("id") Long id) {
		roleService.removeOne(id);
	}

}
