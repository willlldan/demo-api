package com.bts.demo.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bts.demo.dto.ResponseData;
import com.bts.demo.dto.SearchData;
import com.bts.demo.helper.ImageUploadHelper;
import com.bts.demo.models.entities.Product;
import com.bts.demo.services.ProductService;

import net.bytebuddy.utility.RandomString;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;


@RestController
@RequestMapping("/api/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ImageUploadHelper imageUploadHelper;
	
	@Autowired
	private HttpServletResponse response;
	
	
	private static final String UPLOADED_PATH = "C:\\Users\\Will\\Documents\\workspace-spring-tool-suite-4-4.13.1.RELEASE\\demo\\src\\main\\resources\\assets\\";

	
	@GetMapping
	public ResponseEntity<ResponseData<Iterable<Product>>> findAll() {
		
		ResponseData<Iterable<Product>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(productService.findAll());
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseData<Product>> findOne(@PathVariable("id") Long id) {
		
		ResponseData<Product> response = new ResponseData<>();
		response.setStatus(true);
		response.setData(productService.findOne(id));
		
		return ResponseEntity.ok(response);
		
	}

	
	@PostMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Product>> save(@Valid @RequestBody Product product, Errors errors) {
		
		ResponseData<Product> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setStatus(true);
		response.setData(productService.save(product));
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Product>> update(@Valid @RequestBody Product product, Errors errors) {

		ResponseData<Product> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setStatus(true);
		response.setData(productService.save(product));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public void removeOne(@PathVariable("id") Long id) {
		productService.removeOne(id);
	}
	
	
//	Find By name
	@PostMapping("/search/name")
	public ResponseEntity<ResponseData<List<Product>>> findByName(@RequestBody SearchData searcData) {
		
		ResponseData<List<Product>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(productService.findByName(searcData.getSearchKey()));
		
		return ResponseEntity.ok(response);
	}
	
//	Find By Category
	@GetMapping("/search/category/{categoryId}")
	public ResponseEntity<ResponseData<List<Product>>> findByCategory(@PathVariable("categoryId") Long categoryId) {
		
		ResponseData<List<Product>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(productService.findByCategory(categoryId));
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/upload/{id}")
	public ResponseEntity<ResponseData<Product>> uploadImage(@RequestParam(name = "file", required = false) MultipartFile file, @PathVariable("id") Long id) {
		ResponseData<Product> response = new ResponseData<>();
		
		if(file == null) {
			response.setData(null);
			response.setCode("500");
			response.getMessage().add("Please select a file");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		if(!imageUploadHelper.validationImage(file).isEmpty()) {
			response.setData(null);
			response.setCode("500");
			response.setMessage(imageUploadHelper.validationImage(file));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		Product product = productService.findOne(id);
		
		
		try {
			byte[] bytes = file.getBytes();
			String name = RandomString.make() + "_" +file.getOriginalFilename();
			Path path = Paths.get(UPLOADED_PATH + name);
			
			Files.write(path, bytes);
			
			product.setImage(name);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		response.setStatus(true);
		response.setCode("200");
		response.setData(productService.save(product));
		
		
		return ResponseEntity.ok(response);
		
	}
	
	@GetMapping("/reports")
	public void getReport() throws Exception {
		
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd-HH-mm-ss").format(new Date());
		String fileName = "product_list_" + timeStamp + ".pdf";
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		JasperPrint jasperPrint = productService.generateJasperPrint();
		JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
	}
	
	@GetMapping("/reports/{id}")
	public void getReport(@RequestParam("id") Long id) throws Exception {
		
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String fileName = "product_list_by_category" + timeStamp + ".pdf";
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		JasperPrint jasperPrint = productService.reportByCategory(id);
		JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
	}
		
	
	
}
