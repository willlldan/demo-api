package com.bts.demo.helper;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Category;
import com.bts.demo.models.entities.Product;
import com.bts.demo.models.repository.CartRepository;
import com.bts.demo.models.repository.CategoryRepository;
import com.bts.demo.models.repository.ProductRepository;

@Component
public class DemoData {
	
	@Autowired
	private CategoryRepository categoryRepo;
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private CartRepository cartRepo;
	
	
	@EventListener
	public void appReady(ApplicationReadyEvent event) {
			
			if(categoryRepo.count() == 0) {
				categoryRepo.save(new Category("Accessories"));
				categoryRepo.save(new Category("Monitor"));
				categoryRepo.save(new Category("Mouse"));
				categoryRepo.save(new Category("Keyboard"));				
			}
			
			if(productRepo.count() == 0) {
				// Monitor
				productRepo.save(new Product("Asus TUF 2283XS", "32 inch monitor", 300, new Category((long) 2, "Monitor")));
				productRepo.save(new Product("BenQ limiter Series","40inch montiro with 144hz", 500, new Category((long) 2, "Monitor")));
				productRepo.save(new Product("Dell S43523","Curve monitor with 43inch size", 9990, new Category((long) 2, "Monitor")));
				productRepo.save(new Product("Polytron mini s21+","12inch monitor with 144Hz", 20, new Category((long) 2, "Monitor")));
				
				// Mouse				
				productRepo.save(new Product("Logitech G102 Mouse Gaming Wired","Mouse gaming with wired & RGB Feature", 200, new Category((long) 3, "Mouse")));
				productRepo.save(new Product("Logitech M330 Mouse Gaming Wireless","Mouse gaming with Wireless & RGB Feature", 250, new Category((long) 3, "Mouse")));
				productRepo.save(new Product("Logitech MX Ergo Mouse","Mouse with ergonomic design", 10000, new Category((long) 3, "Mouse")));
				productRepo.save(new Product("Razer Naga Pro Mouse Gaming Wired","Mouse gaming with wired & RGB Feature", 500, new Category((long) 3, "Mouse")));
				//  Keyboard
				productRepo.save(new Product("Keychron Q1 Mechanical Keyboard","Mechanical keyboard with blue switch", 300, new Category((long) 4, "Keyboard")));
				productRepo.save(new Product("Fantech MK857 Mechanical Keyboard","Mechanical keyboard with 'economic price'", 250, new Category((long) 4, "Keyboard")));
				productRepo.save(new Product("Vortex Series GT-8 Mechanical Keyboard","Best mechanical keyboard in the world", 10000, new Category((long) 4, "Keyboard")));
				productRepo.save(new Product("Red Dragon Mechanical Keyboard","Mechanical keyboard with spirit of dragon", 99999, new Category((long) 4, "Keyboard")));
			}
			
			if(cartRepo.count() == 0) {		
				cartRepo.save(new Cart("Wildan Fauzi Rakhman", "Majalengka", "willlldan.fauzi@gmail.com"));
				cartRepo.save(new Cart("Tasha Rahman Anawara", "Garut", "tasha.rahman@gmail.com"));
			}

		
	}
}
