package com.bts.demo.dto;

import javax.validation.constraints.NotEmpty;

public class CartData {

	private Long id;
	
	
	@NotEmpty(message="Name is required")
	private String name;
	
	@NotEmpty(message="Address is required")
	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
